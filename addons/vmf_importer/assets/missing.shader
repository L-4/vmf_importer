shader_type spatial;
render_mode unshaded;

uniform float square_scale = 1000.0f;

void fragment()
{
    float total = floor(SCREEN_UV.x * VIEWPORT_SIZE.x * square_scale) +
                  floor(SCREEN_UV.y * VIEWPORT_SIZE.y * square_scale);
    ALBEDO = mod(total,2.0) == 0.0 ? vec3(1.0,0.0,1.0) : vec3(0.0,0.0,0.0);
}