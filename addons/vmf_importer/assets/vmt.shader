shader_type spatial;
render_mode blend_mix, diffuse_toon, cull_back;

uniform sampler2D albedo;
uniform vec2 scale = vec2(1.0f);


void fragment()
{
	vec4 pixel = texture(albedo, mod(UV * scale, 1.0));
	ALBEDO = pixel.rgb * COLOR.rgb;
}