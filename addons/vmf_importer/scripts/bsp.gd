tool
extends Reference

# Libraries
var VMT
var Option
var rlib

const DEBUG_COLORS := [
	Color.red,
	Color.blue,
	Color.green,
	Color.cyan
]

var debug_shader: ShaderMaterial
var missing_material: ShaderMaterial

var options: Dictionary
func _init(_options: Dictionary) -> void:
	options = _options
	VMT = preload("vmt.gd").new(options)
	Option = preload("option_lib.gd")
	rlib = preload("regex_library.gd").new()
	missing_material = load("res://addons/vmf_importer/assets/missing.material")
	var shader = preload("res://addons/vmf_importer/assets/debug.shader")
	debug_shader = ShaderMaterial.new()
	debug_shader.shader = shader

	plane_regex = RegEx.new()
	assert(OK == plane_regex.compile("^\\((-?[\\d\\.]+) (-?[\\d\\.]+) (-?[\\d\\.]+)\\) \\((-?[\\d\\.]+) (-?[\\d\\.]+) (-?[\\d\\.]+)\\) \\((-?[\\d\\.]+) (-?[\\d\\.]+) (-?[\\d\\.]+)\\)$"))


var plane_regex: RegEx
func parse_plane_string(plane_string: String) -> Option:
	var result = plane_regex.search(plane_string)

	if result:
		assert(result.get_group_count() == 9)

		var vertices := []

		for i in range(1, 10):
			vertices.append(result.strings[i].to_float())

		return Option.some(Plane(
			Vector3(vertices[1],vertices[2],vertices[0]),
			Vector3(vertices[4],vertices[5],vertices[3]),
			Vector3(vertices[7],vertices[8],vertices[6])
		))
	else:
		return Option.none("Could not parse plane!")

class TextureMapper:
	# Helper class for parsing the "uaxis"/"vaxis" parameters and generating UV coords based off of them
# TODO: Big optimization: stop it from recompiling regex each time
	enum { X, Y, Z, OFFSET, SCALE, NUM_PARAMS }

	var u: Array = []
	var v: Array = []

	func _init(u_string: String, v_string: String) -> void:
		var uv_parser := RegEx.new()
		assert(uv_parser.compile("^\\[(-?[\\d\\.]+) (-?[\\d\\.]+) (-?[\\d\\.]+) (-?[\\d\\.]+)\\] (-?[\\d\\.]+)$") == OK)
		if not u.empty() and v.empty():
			printerr("Tried to set uvs twice")
			return
		var result = uv_parser.search(u_string)
		if result:
			for i in range(NUM_PARAMS):
				u.append(result.strings[i + 1].to_float())
		else:
			printerr(str("Could not parse u: ", u_string))
			return

		result = uv_parser.search(v_string)
		if result:
			for i in range(NUM_PARAMS):
				v.append(result.strings[i + 1].to_float())
		else:
			printerr(str("Could not parse v: ", v_string))
			return

	func get_coordinates(p: Vector3) -> Vector3:
		if u.empty() or v.empty():
			return Vector3()
		return Vector3(
			# Reminder that hammer's coordinates are y, z, x
			(u[Y] * p.x + u[Z] * p.y + u[X] * p.z) / u[SCALE] + u[OFFSET],
			(v[Y] * p.x + v[Z] * p.y + v[X] * p.z) / v[SCALE] + v[OFFSET],
			0
		)

class ClockwiseSorter:
	var center: Vector2

	func _init(c: Vector2): center = c

	func sort(a: Vector2, b: Vector2) -> bool:
		if a.x - center.x >= 0 and b.x - center.x < 0: return true
		if a.x - center.x < 0 and b.x - center.x >= 0: return false

		if a.x - center.x == 0 and b.x - center.x == 0:
			if a.y - center.y >= 0 or b.y - center.y >= 0:
				return a.y > b.y
			return b.y > a.y

		return (center - a).cross(center - b) < 0

const VERTEX_PROMIXITY_CULL := 0.005 # This may need tweaking if degenerate face issues show up

func get_plane_convex_polygon(planes: Array, i: int) -> Array:
	var plane: Plane = planes[i]
	var vertices = []
	for j in range(planes.size()):    # For each other plane combination d1, d2
		if j == i: continue             #
		for k in range(planes.size()):  #
			if k == i or k == j: continue #

			var intersection = plane.intersect_3(planes[j], planes[k])
			if intersection: # If a, d1, and d2 intersect
				for l in range(planes.size()):            # For each plane l that is not i, j or k
					if l == i or l == j or l == k: continue #
					# Above is clockwise, both in vmf and with the Plane class
					# Or is it counter clockwise in Plane?
					# Note: Plane.is_point_over() defaults to false if point is in plane (I think?)
					# For this reason adding plane[l].normal * 0.0001 isn't (shouldn't be) required
					if planes[l].is_point_over(intersection): # If intersection is above plane
						intersection = null # Discard
						break
					else:
						for v in vertices: # TODO: Test whether it is worth checking for equality or just jumping to distance test
							if intersection == v or intersection.distance_to(v) <= VERTEX_PROMIXITY_CULL:
								intersection = null
								break
						if not intersection:
							break
				if intersection: # Else append to array
					vertices.append(intersection) # ...add intersection to list
	# Vertices should now be a list of vertices that
	# are the corners to a convex polygon on plane a
	return vertices

func translate_vertices_to_xy_plane(from_plane: Plane, vertices: Array) -> Array:
		if from_plane.normal == Plane.PLANE_XZ.normal: return vertices.duplicate()
		var basis := Basis()
		basis.y = from_plane.normal
		basis.x = (from_plane.project(Vector3(1, 1, 1))
			- from_plane.center()).normalized()
		basis.z = basis.y.cross(basis.x)

		var rotator = Transform(basis, from_plane.center())

		var transformed_vertices := []
		for vertex in vertices: transformed_vertices.append(rotator.xform_inv(vertex))
		return transformed_vertices

func quad_interp(a: Vector3, b: Vector3, c: Vector3, d: Vector3, u: float, v: float) -> Vector3:
	var abu := a.linear_interpolate(b, u);
	var dcu := d.linear_interpolate(c, u);
	return abu.linear_interpolate(dcu, v);

func mesh_from_solid(solid: Dictionary) -> ArrayMesh:
	var mesh = ArrayMesh.new()

	var planes := []
	var has_displacement := false
	for side in solid.side:
		if side.has("dispinfo"): has_displacement = true
		planes.append(parse_plane_string(side.plane).get_or_err())

	for i in range(planes.size()):
		var plane = planes[i]
		var side = solid.side[i]
		if side.material.find("tool") >= 0: continue # Skip tool textures for now TODO: Blacklist materials

		var vertices := get_plane_convex_polygon(planes, i)
		var transformed_vertices := translate_vertices_to_xy_plane(plane, vertices)
		# Transform complete

		var vertices_without_y := []
		for vertex in transformed_vertices: vertices_without_y.append(Vector2(vertex.x, vertex.z))
		# Drop complete

		# Homebrew sort clockwise
		var center = Vector2()
		for vertex in vertices_without_y:
			center += vertex
		center /= vertices_without_y.size()

		var sorted_vertices := vertices_without_y.duplicate()
		sorted_vertices.sort_custom(ClockwiseSorter.new(center), "sort")

		var index_map := []
		for j in range(sorted_vertices.size()):
			for k in range(vertices_without_y.size()):
				if sorted_vertices[j] == vertices_without_y[k]:
					index_map.append(k)
					break

		assert(vertices_without_y.size() == index_map.size())

		var o_vertices := PoolVector3Array()
		var o_indices := PoolIntArray()
		var o_normals := PoolVector3Array()
		var o_colors := PoolColorArray()

		# What will be different if rendered as displacement:
		# Vertex positions, indices, normals

		if side.has("dispinfo") and options.get("import_displacements"):
			var dispinfo = side.dispinfo[0]
			if not vertices.size() == 4: printerr("Wrong number of vertices in displacement!")
			var power = dispinfo.power.to_float()
			var displacement_origin = rlib.parse_vector_in_brackets(dispinfo.startposition).unwrap()
			var temp_origin = displacement_origin
			displacement_origin.x = temp_origin.y
			displacement_origin.y = temp_origin.z
			displacement_origin.z = temp_origin.x

			var start_vertex := -1
			for j in range(vertices.size()):
				if displacement_origin.distance_to(vertices[index_map[j]]) < 0.05:
					start_vertex = j
					break

			if start_vertex == -1: printerr("No vertex was at displacement origin")
			# Number of verts in side = 2 ^ power + 1
			# Number of edges in side = 2 ^ power
			var side_edges: float = pow(2, power)
			var side_verts: float = pow(2, power) + 1
			var bl: Vector3 = vertices[index_map[(0 + start_vertex) % 4]]
			var br: Vector3 = vertices[index_map[(1 + start_vertex) % 4]]
			var tr: Vector3 = vertices[index_map[(2 + start_vertex) % 4]]
			var tl: Vector3 = vertices[index_map[(3 + start_vertex) % 4]]

			var normals = rlib.parse_displacement_data(dispinfo.normals[0], power, true).unwrap()
			var distances = rlib.parse_displacement_data(dispinfo.distances[0], power).unwrap()

			for x in range(side_verts):

				for y in range(side_verts):
					var nx: int = x
					var ny: int = y
					if plane.normal == Plane.PLANE_XZ.normal: # Fix for that weird xz plane bug
						nx = y
						ny = x

					var normal = normals[nx][ny]
					var distance = distances[nx][ny]
					# (x and y) / side_edges to make the range 0-1
					var vertex: Vector3 = quad_interp(bl, br, tr, tl, x / side_edges, y / side_edges)

					o_vertices.append(vertex + normal * distance)
					o_normals.append(plane.normal)


			for x in range(side_edges):
				for y in range(side_edges):
					o_indices.append(x * side_verts + y)
					o_indices.append((x + 1) * side_verts + y)
					o_indices.append(x * side_verts + (y + 1))

					o_indices.append(x * side_verts + (y + 1))
					o_indices.append((x + 1) * side_verts + y)
					o_indices.append((x + 1) * side_verts + (y + 1))

#			for j in range(0, o_indices.size(), 3):
#				var temp_plane = Plane(
#					o_vertices[o_indices[j+0]],
#					o_vertices[o_indices[j+1]],
#					o_vertices[o_indices[j+2]]
#				)
#
#				o_normals[o_indices[j+0]] += temp_plane.normal
#				o_normals[o_indices[j+1]] += temp_plane.normal
#				o_normals[o_indices[j+2]] += temp_plane.normal
#
#			for j in range(o_normals.size()): o_normals[j] = o_normals[j].normalized()

			for j in range(o_vertices.size()): o_colors.append(DEBUG_COLORS[start_vertex])

		else:
			if not options.get("import_brushes"): continue
			if has_displacement: continue
			o_vertices = PoolVector3Array(vertices)

			for j in range(1, vertices.size() - 1):
				o_indices.append(0)
				o_indices.append(j)
				o_indices.append(j+1)
				o_colors.append(Color.white)

			# One index for each poly in first triangle, then one triangle per extra poly
			assert((o_indices.size() - 3) / 3 == vertices.size() - 3)

			# Map indices to the right vertices
			for j in range(o_indices.size()):	o_indices[j] = index_map[o_indices[j]]

			# "Normal generation"
			for _i in range(vertices.size()): o_normals.append(plane.normal)

		# HACK TIME:
		# This hack might not always work, and I don't know why it would in the first place. But it does.
		if plane.normal == Plane.PLANE_XZ.normal: o_indices.invert()

		# Brush type independant calculations
		# Generate UVs
		var texture_mapper = TextureMapper.new(side.uaxis, side.vaxis)
		var o_uvs = PoolVector3Array()
		for vertex in o_vertices: o_uvs.append(texture_mapper.get_coordinates(vertex))

		var surface_data := []
		surface_data.resize(Mesh.ARRAY_MAX)
		surface_data[Mesh.ARRAY_VERTEX] = o_vertices
		surface_data[Mesh.ARRAY_NORMAL] = o_normals
		surface_data[Mesh.ARRAY_TEX_UV] = o_uvs
		surface_data[Mesh.ARRAY_INDEX]  = o_indices
#		surface_data[Mesh.ARRAY_COLOR]  = o_colors

		mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, surface_data)

		# Material time
		mesh.surface_set_material(mesh.get_surface_count() - 1, VMT.parse(str("res://vmt/", side.material, ".vmt")).get_or(missing_material))

	if options.get("generate_lightmaps"):
		var highest_light_res := 0.0

		for side in solid.side:
			highest_light_res = max(highest_light_res, side.lightmapscale.to_float())

		mesh.lightmap_unwrap(Transform(), 1.0 / highest_light_res)
	return mesh
