tool
extends Reference

var Option
# Default entity
var missing_entity_scene

var options: Dictionary
func _init(_options: Dictionary) -> void:
	options = _options
	Option = preload("option.gd")
	missing_entity_scene = preload("res://addons/vmf_importer/nodes/MissingEntity.tscn")

	for key in regex_helpers.keys():
		var regex = RegEx.new()
		assert(regex.compile(regex_helpers[key]) == OK)
		regex_helpers[key] = regex

	for key in entity_handlers.keys():
		entity_handlers[key] = funcref(self, key)

# TODO: make a parser for arbitrary amounts of numbers, to minimize code length
var regex_helpers := {
	"vector3": "^(-?[0-9\\.]+) (-?[0-9\\.]+) (-?[0-9\\.]+)$",
	"vector4": "^(-?[0-9\\.]+) (-?[0-9\\.]+) (-?[0-9\\.]+) (-?[0-9\\.]+)$"
}

# PARSERS

func parse_vector3(string: String) -> Option:
	var result = regex_helpers.vector3.search(string)

	if not result: return Option.new(false, str("Could not parse vector3 ", string))
	var values = result.strings
	for i in range(1, 4):
		if not values[i].is_valid_float(): return Option.new(false, "Could not parse float")
		values[i] = values[i].to_float()
	return Option.new(true, Vector3(values[2], values[3], values[1]))

func parse_angle(string: String) -> Option:
	var result = regex_helpers.vector3.search(string)

	if not result: return Option.new(false, str("Could not parse angle: ", string))
	var values = result.strings
	for i in range(1, 4):
		if not values[i].is_valid_float(): return Option.new(false, str("Could not parse float in angle: ", string))
		values[i] = values[i].to_float()
	return Option.new(true, Vector3(values[1], values[2], values[3]))

func parse_vector4(string: String) -> Option:
	var result = regex_helpers.vector4.search(string)

	if not result: return Option.new(false, str("Could not parse vector4: ", string))
	var values: Array = result.strings
	for i in range(1, 5):
		if not values[i].is_valid_float(): return Option.new(false, str("Could not parse float in vector4 ", string))
		values[i-1] = values[i].to_float()
	values.pop_back()
	return Option.new(true, values)

# ENTITY CONSTRUCTORS
const BAKE_LIGHT = true

func light(entity_data: Dictionary) -> Option:
	var light = OmniLight.new()
	if options.get("import_lights_for_baking"):
		light.light_bake_mode = Light.BAKE_ALL
		light.hide()

	if entity_data.has("_light"):
		var color_option = parse_vector4(entity_data._light)
		if color_option.is_some:
			var color = color_option.get_value()
			light.light_color = Color(
				color[0] / 255, color[1] / 255, color[2] / 255
			)

			light.light_energy = color[3] / 650

	return Option.new(true, light)

func light_spot(entity_data: Dictionary):
	var light = SpotLight.new()
	if options.get("import_lights_for_baking"):
		light.light_bake_mode = Light.BAKE_ALL
		light.hide()

	if entity_data.has("angles"):
		var angle_option = parse_angle(entity_data.angles)

		if angle_option.is_some:
			light.rotation_degrees = angle_option.get_value()
	else:
		printerr("Spotlight did not have angles!")

	if entity_data.has("_cone"):
		if entity_data._cone.is_valid_float():
			light.spot_angle = entity_data._cone.to_float()
			light.hide()

	if entity_data.has("_light"):
		var color_option = parse_vector4(entity_data._light)
		if color_option.is_some:
			var color = color_option.get_value()
			light.light_color = Color(
				color[0] / 255, color[1] / 255, color[2] / 255
			)

			light.light_energy = color[3] / 650

	if entity_data.has("angles"):
		var angle_option = parse_angle(entity_data.angles)

		if angle_option.is_some:
			light.rotation_degrees = angle_option.get_value()
	else:
		printerr("Spotlight did not have angles!")

	if entity_data.has("_light"):
		var color_option = parse_vector4(entity_data._light)
		if color_option.is_some:
			var color = color_option.get_value()
			light.light_color = Color(
				color[0] / 255, color[1] / 255, color[2] / 255
			)

			light.light_energy = color[3] / 255



	return Option.new(true, light)

func light_environment(entity_data: Dictionary):
	var light = DirectionalLight.new()
	if options.get("import_lights_for_baking"):
		light.light_bake_mode = Light.BAKE_ALL



# Register regex helpers and functions

var entity_handlers := {
	"light": true,
	"light_spot": true
}

# Main function

const CREATE_DEBUG_ENTITIES := false

func create_entity(entity_data: Dictionary) -> Option:
	if entity_handlers.has(entity_data.classname):
		var entity_option = entity_handlers[entity_data.classname].call_func(entity_data)
		if entity_option.is_some:
			var entity = entity_option.get_value()
			entity.name = str(entity_data.classname, "_", entity_data.id)
			if not entity_data.has("origin"):
				printerr("Entity ", entity_data.classname, " does not have origin")
			else:
				var position_option = parse_vector3(entity_data.origin)
				if position_option.is_some:
					entity.transform.origin = position_option.get_value()
			return Option.new(true, entity)
	if not CREATE_DEBUG_ENTITIES:
		return Option.new(false, "", true)
	var missing_entity = missing_entity_scene.instance()
	missing_entity.name = str(entity_data.classname, "_", entity_data.id)
	missing_entity.get_node("Viewport/Label").text = entity_data.classname
	if entity_data.has("origin"):
		var position_option = parse_vector3(entity_data.origin)
		if position_option.is_some:
			missing_entity.transform.origin = position_option.get_value()
	return Option.new(true, missing_entity)
