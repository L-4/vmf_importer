tool
extends Reference
class_name Option

var is_some: bool
var value_or_error

const ALWAYS_ERROR_ON_FAIL: bool = false
const LOG_ALL_ERRORS: bool = true

func _init(_is_some: bool = true, _value_or_error = "No error", silent: bool = false) -> void:
	is_some = _is_some
	value_or_error = _value_or_error

	if silent: return
	if ALWAYS_ERROR_ON_FAIL: get_or_err()
	if LOG_ALL_ERRORS: if not is_some: print(str("Option.is_some = false: ", value_or_error))

func unwrap(): # Use this, not get_or_err()
	if not is_some:
		print("======= FATAL ERROR =======")
		print(value_or_error)
		print("===========================")
#		assert(false)
		return null
	return value_or_error

func get_or_err(): return unwrap()

func get_or(value):
	if is_some: return value_or_error
	return value

func get_value(): return value_or_error

func printerror():
	print("========== ERROR ==========")
	print(value_or_error)
	print("===========================")