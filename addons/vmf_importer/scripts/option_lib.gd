extends Reference

static func some(value) -> Option: return Option.new(true, value)
static func none(error) -> Option: return Option.new(false, error)

static func get_key(dict: Dictionary, key) -> Option:
	if dict.has(key): return some(dict[key])
	return none(str("Could not get key '", key, "'"))

static func add_option_value(dict: Dictionary, key, value: Option) -> bool:
	if value.is_some:
		dict[key] = value.get_value()
		return true
	else: return false