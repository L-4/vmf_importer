tool
extends Reference

var regex := {
	"number": "^(-?[\\d.]+(?:[eE]-?\\d+)?) *",
	"vector_in_brackets": "^\\[(-?[\\d.]+(?:[eE]-?\\d+)?) (-?[\\d.]+(?:[eE]-?\\d+)?) (-?[\\d.]+(?:[eE]-?\\d+)?)\\]$"
}

func _init() -> void:
	for key in regex.keys():
		var r = RegEx.new()
		assert(r.compile(regex[key]) == OK)
		regex[key] = r

# Helpers

func parse_number_list(list: String) -> Array:
	var offset := 0
	var numbers := []
	while offset < list.length():
		var result: RegExMatch = regex.number.search(list.substr(offset, 32))
		if result:
			var number_string: String = result.strings[1].to_lower()
			offset += result.get_end()
			if not number_string.is_valid_float():
				printerr("Could not parse float")
				return numbers
			numbers.append(number_string.to_float())
		else:
			printerr("Failed to parse until end: ", list)
			return numbers
	return numbers

func parse_displacement_data(displacement: Dictionary, power: int, is_vector: bool = false) -> Option:
	var cols = pow(2, power) + 1
	var rows = cols
	if is_vector: cols *= 3

	var data := []

	for i in range(rows):
		var row = str("row", i)
		if not displacement.has(row):
			printerr("Displacement data did not have enough rows.")
			return Option.new(false, "Displacement data did not have enough rows.")
		var numbers := parse_number_list(displacement[row])
		if numbers.size() != cols:
			printerr("Displacement data had wrong number of cols")
			return Option.new(false, str("Displacement data had wrong number of cols: ", numbers.size(), " != ", cols))
		data.append(numbers)

	if is_vector:
		for i in range(data.size()):
			var new_data := []
			var old_data = data[i]
			for j in range(0, old_data.size(), 3):
				# TODO: This is absolutely terrible, it assumes hammer's coordinate system since that is what I'm using it for now
				# Get rekt future me.
				new_data.append(Vector3(old_data[j+1], old_data[j+2], old_data[j]))
			data[i] = new_data
	return Option.new(true, data)

func parse_vector_in_brackets(subject: String) -> Option:
	var result = regex.vector_in_brackets.search(subject)
	if not result: return Option.new(false, "Could not parse vector")

	return Option.new(true, Vector3(result.strings[1].to_float(), result.strings[2].to_float(), result.strings[3].to_float()))