tool
extends Reference
class_name Tokenizer

var regex_chunk_size := 1000
var tokens: Array
const DEBUG := false
const TARGET_TOKEN = 566996
const TOKEN_CONTEXT_RANGE = 15

enum { NAME, REGEX_STRING, DISCARD, REGEX_OBJECT }
var Option

func _init(_tokens: Array) -> void:
	tokens = _tokens
	Option = preload("option.gd")

	for i in range(tokens.size()):
		var token = tokens[i]

		# To some extent make sure that the data is not garbage
		assert(token.size() == 3)
		assert(typeof(token[NAME])         == TYPE_STRING)
		assert(typeof(token[REGEX_STRING]) == TYPE_STRING)
		assert(typeof(token[DISCARD])      == TYPE_BOOL)

		var token_regex := RegEx.new()
		assert(token_regex.compile(str("^", token[REGEX_STRING])) == OK)

		tokens[i].append(token_regex)

func tokenize(string: String) -> Option:
	var offset := 0
	var found_tokens := []

	while offset < string.length():
		var found_any = false
		for token in tokens:
			var result = token[REGEX_OBJECT].search(string.substr(offset, regex_chunk_size))

			if result:
				offset += result.get_end()

#				if result.get_group_count() < 1: # TODO: this should be built into some unit test, not here
#					return Option.none(str("No capture group for pattern '", token[REGEX_STRING], "'"))

				if not token[DISCARD]: found_tokens.append([token[NAME], result.strings[1]])
				if DEBUG:
					if found_tokens.size() == TARGET_TOKEN + TOKEN_CONTEXT_RANGE:
						print("======== Printing info about target token ========")
						for i in range(TOKEN_CONTEXT_RANGE * 2):
							var index = found_tokens.size() - (TOKEN_CONTEXT_RANGE - i) - TOKEN_CONTEXT_RANGE
							print(index, ": [", found_tokens[index][0], ", ", found_tokens[index][1], "]")
				found_any = true
				break
		if not found_any: return Option.new(false, "Entire string could not be parsed")

	return Option.new(true, found_tokens)