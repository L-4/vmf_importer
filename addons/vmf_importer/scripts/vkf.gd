tool
extends Reference

const DEBUG := false

var tokenizer: Tokenizer
var options: Dictionary
func _init(_options: Dictionary) -> void:
	options = _options
	tokenizer = preload("tokenizer.gd").new([
			["space", "([\\t ]+)", false],
			["newline", "(\\n[\\n\\s]*)", true],
			["value", "\"([^\"]*)\"", false],
			["value", "([%$\\da-zA-Z][^ \\n]*)", false],
			["comment", "(//[^\n]*)", true],
			["openbracket", "([{])", false],
			["closebracket", "([}])", false],
		])

func parse(file: String) -> Option:
	var vkf_file = File.new()
	var status = vkf_file.open(file, File.READ)
	if status != OK: return Option.new(false, str("Failed to open file '", file, "' with error #", status), true)

	var vkf_tokens = tokenizer.tokenize(vkf_file.get_as_text().to_lower().replace("\\", "/"))
	if vkf_tokens.is_some:
		vkf_tokens = vkf_tokens.get_value()
	else:
		return vkf_tokens
	vkf_file.close()

	var parsed := {}
	var stack := [
		parsed
	]
	var reader := 0

	var stack_keys := []

	while reader < vkf_tokens.size():
		if DEBUG:
			print("========== reader at ", reader, " ==========")
			for i in range(5):
				print(reader + i, ": [", vkf_tokens[reader+i][0], ", ", vkf_tokens[reader+i][1], "]")

		var token = vkf_tokens[reader]

		match token[0]:
			"value":
				var next_token = vkf_tokens[reader + 1]

				match next_token[0]:
					"openbracket": # Descends one level deeper into the structure
						if not stack.back().has(token[1]):
							if DEBUG: print("Added token '", token[1], "' to stack")
							stack.back()[token[1]] = []
						elif DEBUG:
							print("Token ", token[1], " already on stack")
						if DEBUG:
							if typeof(stack.back()[token[1]]) != TYPE_ARRAY:
								print("Probably about to crash, here's some info:")
								print("Stack: ", stack_keys)
								print("last value on stack: ", stack.back()[token[1]])
								print("type: ", typeof(stack.back()[token[1]]))
						stack.back()[token[1]].append({})
						stack.append(stack.back()[token[1]].back())
						stack_keys.append(token[1])

						reader += 2 # Parsed new element name and open bracket

					"space":
						var next_next_token = vkf_tokens[reader + 2]

						assert(next_next_token[0] == "value")

						stack.back()[token[1]] = next_next_token[1]

						reader += 3 # Parsed key, space, value

#warning-ignore:unassigned_variable
					var next_type:
						printerr(str("Got ", next_type, " after value, which is incorrect"))
			"closebracket":
				stack.pop_back()
				stack_keys.pop_back()

				reader += 1 # Parsed end bracket

#warning-ignore:unassigned_variable
			var unknown_type:
				printerr(str("Got unhandled type: ", unknown_type))
				reader += 1

	return Option.new(true, parsed)