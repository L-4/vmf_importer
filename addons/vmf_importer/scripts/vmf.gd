tool
extends Reference

var VKF
var BSP
var ENT

var options: Dictionary
func _init(_options: Dictionary) -> void:
	options = _options
	VKF = preload("vkf.gd").new(options)
	BSP = preload("bsp.gd").new(options)
	ENT = preload("ent.gd").new(options)

func set_owner_recursive(owner: Node, subject: Node):
	for child in subject.get_children():
		set_owner_recursive(owner, child)
	subject.owner = owner

func parse(path: String) -> Spatial:
	var vmf_tokens = VKF.parse(path).get_or_err()

	var container = Spatial.new()

	var world := Spatial.new()
	world.name = "world"
	container.add_child(world)

	var entities = Spatial.new()
	entities.name = "entities"
	container.add_child(entities)

	for entity_data in vmf_tokens.entity:
		if entity_data.has("classname"):
			if entity_data.classname == "func_detail":
				if options.get("import_func_detail"):
					for solid in entity_data.solid:
						var mesh_instance = MeshInstance.new()
						mesh_instance.mesh = BSP.mesh_from_solid(solid)
						mesh_instance.name = str("brush_", solid.id)
						mesh_instance.use_in_baked_light = true
						world.add_child(mesh_instance)
			else:
				if options.get("import_entities"):
					var entity_option = ENT.create_entity(entity_data)
					if entity_option.is_some:
						entities.add_child(entity_option.get_value())

	for solid in vmf_tokens.world[0].solid:
		var mesh_instance = MeshInstance.new()
		mesh_instance.mesh = BSP.mesh_from_solid(solid)
		mesh_instance.name = str("brush_", solid.id)
		mesh_instance.use_in_baked_light = true
		world.add_child(mesh_instance)

	for child in container.get_children(): set_owner_recursive(container, child)

	return container