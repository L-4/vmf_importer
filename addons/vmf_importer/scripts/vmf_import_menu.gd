tool
extends EditorImportPlugin

func import(source_file: String, save_path: String, options: Dictionary, platform_variants: Array, gen_files: Array) -> int:
#	if not options.has("should_import") and options.should_import: return FAILED

	print("Starting import...")
	var start := OS.get_ticks_msec()

	var scene = PackedScene.new()
	var VMF = preload("vmf.gd").new(options)
	var container = VMF.parse(source_file)
	container.name = source_file.get_file().get_basename()
	scene.pack(container)
	container.free()
	print("Import complete in ", OS.get_ticks_msec() - start, "ms")
	return ResourceSaver.save(str(save_path, ".", get_save_extension()), scene)

const PRESETS = [
	{
		"name": "Default",
		"options": [
			{
				"name": "General", "default_value": "", "usage": PROPERTY_USAGE_GROUP
			},
			{
				"name": "VMT_folder",
				"default_value": "vmt/",
				"property_hint": PROPERTY_HINT_DIR
			},
			{
				"name": "import_lights_for_baking",
				"default_value": false
			},
			{
				"name": "ignore_tool_brushes",
				"default_value": true
			},
			{
				"name": "Fast import options", "default_value": "", "usage": PROPERTY_USAGE_GROUP
			},
			{
				"name": "import_entities",
				"default_value": true
			},
			{
				"name": "import_func_detail",
				"default_value": true
			},
			{
				"name": "import_displacements",
				"default_value": true
			},
			{
				"name": "import_brushes",
				"default_value": true
			},
			{
				"name": "generate_lightmaps",
				"default_value": false
			},
			{
				"name": "import_threads",
				"default_value": 1
			}
		]
	}
]

func get_import_options(preset: int) -> Array: return PRESETS[preset].options
#func get_import_order() -> int:
func get_importer_name() -> String: return "0xl4.vmfimporter"
func get_option_visibility(option: String, options: Dictionary) -> bool: return true
func get_preset_count() -> int: return PRESETS.size()
func get_preset_name(preset: int) -> String: return PRESETS[preset].name
#func get_priority() -> float:
func get_recognized_extensions() -> Array: return ["vmf"]
func get_resource_type() -> String: return "PackedScene"
func get_save_extension() -> String: return "tscn"
func get_visible_name() -> String: return "VMF Importer"
