tool
extends Reference

var VKF
var Option

var options: Dictionary
func _init(_options: Dictionary) -> void:
	options = _options
	VKF = preload("vkf.gd").new(options)
	Option = preload("option.gd")
	vmt_shader = preload("res://addons/vmf_importer/assets/vmt.shader")

var vmt_shader: Shader

var cached_materials := {}

func parse(path: String) -> Option:
	assert(path.get_extension() == "vmt")
	if cached_materials.has(path):
		var cached = cached_materials[path]
		if typeof(cached) == TYPE_BOOL:
			return Option.new(false, "Material failed to load in past", true)
		else:
			return Option.new(true, cached)

	var vmt_tokens = VKF.parse(path)
	if vmt_tokens.is_some:
		vmt_tokens = vmt_tokens.get_value()
	else:
		cached_materials[path] = false
		return vmt_tokens

	var material_properties

	if vmt_tokens.has("lightmappedgeneric"):
		material_properties = vmt_tokens["lightmappedgeneric"][0]

	if vmt_tokens.has("worldvertextransition"):
		material_properties = vmt_tokens["worldvertextransition"][0]

	if material_properties:
		var material = ShaderMaterial.new()
		material.shader = vmt_shader

		if material_properties.has("$basetexture"):
			var texture_path = str("res://png/", material_properties["$basetexture"], ".png")
			var d = Directory.new()
			if d.file_exists(texture_path):
				var texture: Texture = load(texture_path)
				material.set_shader_param("scale", Vector2(1.0 / texture.get_width(), 1.0 / texture.get_height()))
				material.set_shader_param("albedo", texture)
			else:
				cached_materials[path] = false
				return Option.new(false, str("No texture for material ", path, " exists at ", texture_path))
		else:
			cached_materials[path] = false
			return Option.new(false, str("Material '", path, "' doesn't have a basetexture!"))

		cached_materials[path] = material
		return Option.new(true, material)
	else:
		cached_materials[path] = false
		return Option.new(false, str("Unknown shader type. Got: ", vmt_tokens.keys().front(), ". In ", path))
