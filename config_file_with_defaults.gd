tool
extends ConfigFile
# Extends ConfigFile to have default values

var defaults := {}

func load_with_defaults(path: String, _defaults: Dictionary) -> int:
	defaults = _defaults
	return .load(path)

func save_with_defaults(path: String) -> int:
	for section in defaults.keys():
		for key in defaults[section]:
			if not .get_value(section, key) == null:
				.set_value(section, key, defaults[section][key])
	return .save(path)

func load(path: String) -> int:
	return load_with_defaults(path, {})

func reset_section(section: String) -> void:
	pass

func get_section_keys(section: String) -> PoolStringArray:
	var default_keys := PoolStringArray()
	if defaults.has(section):
		default_keys = PoolStringArray(defaults[section].keys())
	var keys = .get_section_keys(section)
	keys.append_array(default_keys)
	return keys

func get_sections() -> PoolStringArray:
	var sections = .get_sections()
	sections.append_array(PoolStringArray(defaults.keys()))
	return sections

func get_value(section: String, key: String, default=null):
	if .get_value(section, key): return .get_value(section, key)
	if defaults.has(section):
		if defaults[section].has(key): return defaults[section][key]
	return default

func has_section_key(section: String, key: String) -> bool:
	return .has_section_key(section, key) or defaults.has(section) and defaults[section].has(key)