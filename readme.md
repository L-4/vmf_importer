VMF importer for the Godot engine
=================================

## Current features:
* Parse .vmf files, and convert brushes to meshes
* Parse .vmt files, load them into godot materials, and map them to the brushes.
* Parse a few entities, convert them to Godot entities and load them into the map
* Save processed .vmf file as scene

## Usage:
(Not in a final state at all, it's kinda hacky. I will soon include some testing materials so that you don't have to extract stuff yourself)

Put .vmf files in /maps, .vmt files in /vmt, png versions of .vtf files in /png. Then hope that it works. Writing this makes me realize how bad this system is.

## Features that would be easy(-ish) to implement:
* Add handlers for more entities. Each entity is a tiny job, but there are a lot of them
* Multithreaded loading. This actually shouldn't be too hard due to Godot's multithreading model
* Automate model conversion to .gltf. Models are stored as .sdm files inside of .mdl files inside of .vpk files. None of those files are useful, but there is [an addon to import .smd files to blender](https://developer.valvesoftware.com/wiki/Blender_Source_Tools), and [a tool to batch convert .vpks to .smd files](https://developer.valvesoftware.com/wiki/Crowbar) (albeit in two steps)
* Parse and render displacements
* Optimize meshes. Currently, each brush is a separate mesh, where each face is a different surface (with potentially a different material). There is any number of ways that this could be improved, but the most sane would probably be to simply combine meshes near each other
* Create static physics for the map
* Triggers. I don't actually know where this goes on this list, but emulating a few of the triggers used in the source engine could allow for some interesting things (automatic door opening etc)
* Make a tool to batch convert materials, as to not have to recompile materials each time.
* INI file for toggling features such as per-stage debug mode
* Incremental "compiling" could be very useful as it results in a lot of downtime while testing features. This could also mean that lightmaps could be left alone if only entities need to be recompiled.
* Proper exporting for scenes to a folder. This should include all required materials (optionally embedded) so that the maps could in theory be used in other projects
* A proper UI for the whole system. Either a toolbar or a custom node. In any case, this would require making a final-ish version of the API.
* Removing the usage of so many autoloads. I didn't do much research when designing the current relationship between scripts. The reason it is structured like it is is that autoloads were the only way I could find to make singletons. 


## Features that would be really really hard to implement:
These are issues that would be really cool to implement, but I am unlikely to be able to do in the near future.

### The void:

Crash course on how source works for the uninformed:
This exporter uses vmf files, whereas games using the source engine use bsp files. bsp files are the compiled version of the vmf files, where the vmf files are only really meant to be used in the [Hammer editor](https://developer.valvesoftware.com/wiki/Valve_Hammer_Editor).

The world is composed of brushes, a convex polygon described by a series of planes, the sides and corners being where the planes intersect. ([more in depth explanation](https://developer.valvesoftware.com/wiki/Valve_Map_Format#Planes)).

Since the whole world is composed of these brushes, the compiler can know what faces do not face the inside of the world, and can cull them at compile time. This information is not present in the vmf. Below is an example of the map ctf_turbine first in tf2, and then in Godot. You can see into the vent on the left in tf2, but not in Godot, due to the fact that those faces could be culled for use in the source engine.

Herein lies the problem. It is not possible to see these faces, as long as you do not leave the map. It would however be a very difficult task to cull these faces in Godot, requiring knowledge in math that I simply don't have.

On the flipside, this problem could simply be ignored. Since none of these faces can ever be seen, it will just result in worse performance, which is not the end of the world. Nevertheless, it is an issue that I would love to see fixed.

### Animations

I don't have a clue. I don't want to think about this one. [Read and weep](https://developer.valvesoftware.com/wiki/MDL)